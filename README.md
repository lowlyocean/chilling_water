# chilling_water
A simple GUI for calculating time needed to cool a beverage

![](chilling_water.png)

![](https://wikimedia.org/api/rest_v1/media/math/render/svg/0d83697a22ce1119485735a17913b412de627ee9)

![](https://wikimedia.org/api/rest_v1/media/math/render/svg/658d6a64b03f64cc54f852f291ca4445bdf67f8d)

https://en.wikipedia.org/wiki/Newton%27s_law_of_cooling