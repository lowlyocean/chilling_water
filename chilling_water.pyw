#!/usr/bin/python3

from math import log
from tkinter import Tk, Entry, Label

ha = 0.237
# hA ~ heat transfer coefficient
c = 4.186 * 556.6  # convert to J/(kgF)


def m(floz):
    return floz * 0.02957


def r(floz):
    return (ha) / (m(floz) * c)


def get_time(_event):
    floz = float(oz_field.get())
    T0 = float(T0_field.get())
    Tenv = float(Tenv_field.get())
    desiredTf = float(desiredTf_field.get())
    cooltime = -log((desiredTf - Tenv) / (T0 - Tenv)) / (60 * r(floz))
    output_field.configure(text=f"Time needed to cool: {cooltime:.1f} minutes.")


if __name__ == "__main__":
    root = Tk()
    root.title("Liquid Chilling")
    ozlabel = Label(root, text="Fluid Ounces:")
    T0label = Label(root, text="Starting Temperature (F):")
    Tenvlabel = Label(root, text="Environment Temperature (F):")
    desiredTflabel = Label(root, text="Desired Temperature (F):")
    output_field = Label(root, text="Time needed to cool: ")

    ozlabel.grid(row=0, sticky="w", column=0)
    T0label.grid(row=1, sticky="w", column=0)
    Tenvlabel.grid(row=2, sticky="w", column=0)
    desiredTflabel.grid(row=3, sticky="w", column=0)
    output_field.grid(row=4, sticky="w", column=0, columnspan=2)

    oz_field = Entry(root, justify="right", width=3)
    T0_field = Entry(root, justify="right", width=3)
    Tenv_field = Entry(root, justify="right", width=3)
    desiredTf_field = Entry(root, justify="right", width=3)

    oz_field.grid(row=0, column=1)
    oz_field.bind('<Return>', get_time)
    T0_field.grid(row=1, column=1)
    T0_field.bind('<Return>', get_time)
    Tenv_field.grid(row=2, column=1)
    Tenv_field.bind('<Return>', get_time)
    desiredTf_field.grid(row=3, column=1)
    desiredTf_field.bind('<Return>', get_time)

    root.mainloop()
